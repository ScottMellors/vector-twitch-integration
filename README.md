## Getting Started

Step 1: Get the **Anki Vector SDK** [here](https://developer.anki.com/vector/docs).

Step 2: Follow the guide for setting up a **Twitch Bot** [here](https://dev.twitch.tv/docs/irc/). 

Step 3: Add your credentials from step 2 into the **Constants.js** file.

Step 4: Run the **bot.js** as described in step 2.

Step 5: **Profit**

## Commands

**Hello User!** - Vector can greet users who say "hello vector" in chat.

**Wheelie** - Vector will perform a feat of robotic prowess after a user says "pop a wheelie vector"

**Eye Colour Change** - Vector will temporarily change it's eye colour based on what a user requests: "change your eye colour to " appended with colours purple / teal / orange / yellow / lime / sapphire / random. 

## Future Developments

**Thank user on bits / sub event**

**Custom Dance**

**Streamlabs Support**

## Contact

Any issues with inital setup, feature requests or general banter, contact me here: [@GhostlyTuna](http://twitter.com/ghostlytuna) / [Twitch](http://twitch.tv/ghostlytuna)

Pull Requests will be open!