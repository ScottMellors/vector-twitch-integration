#!/usr/bin/env python3

# Copyright (c) 2018 Anki, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License in the file LICENSE.txt or at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Set Vector's eye color.
"""
import time
import anki_vector
import random
import argparse
import sys

def main():
        parser = argparse.ArgumentParser()
        parser.add_argument("--colour")

        args = anki_vector.util.parse_command_args(parser)
        with anki_vector.Robot(args.serial) as robot:
                
                colour = sys.argv[2]

                if colour == "purple":
                        robot.behavior.set_eye_color(hue=0.83, saturation=0.76)
                        time.sleep(5)
                elif colour == "teal":
                        robot.behavior.set_eye_color(hue=0.42, saturation=1.00)
                        time.sleep(5)
                elif colour == "orange":
                        robot.behavior.set_eye_color(hue=0.05, saturation=0.95)
                        time.sleep(5)
                elif colour == "yellow":
                        robot.behavior.set_eye_color(hue=0.21, saturation=1.00)
                        time.sleep(5)
                elif colour == "lime":
                        robot.behavior.set_eye_color(hue=0.57, saturation=1.00)
                        time.sleep(5)
                elif colour == "sapphire":
                        robot.behavior.set_eye_color(hue=0.83, saturation=0.76)
                        time.sleep(5)
                elif colour == "random":
                        robot.behavior.set_eye_color(hue=random.uniform(0, 1), saturation=random.uniform(0, 1))
                        time.sleep(5)
                else:
                        robot.behavior.say_text("I don't know that colour, sorry")      

                

if __name__ == '__main__':
    main()
