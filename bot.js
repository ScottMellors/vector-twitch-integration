const tmi = require('tmi.js');

var constants = require('./constants');

const HELLO_VECTOR = "hello vector";
const EYE_COLOUR = "change your eye colour to ";
const WHEELIE = "pop a wheelie vector";

const HELLO_VECTOR_COOLDOWN = 30;
const EYE_COLOUR_COOLDOWN = 30;
const WHEELIE_COOLDOWN = 60;

// Define configuration options
const opts = {
  identity: {
    username: constants.BOT_USERNAME,
    password: constants.OAUTH_TOKEN
  },
  channels: [
    constants.CHANNEL_NAME
  ]
};

var lastUsedArray = new Map();

// Create a client with our options
const client = new tmi.client(opts);

// Register our event handlers (defined below)
client.on('message', onMessageHandler);
client.on('connected', onConnectedHandler);

// Connect to Twitch:
client.connect();

function checkCooldown(command, commandCooldown) {
  if (lastUsedArray.has(command)) {
    var dif = new Date().getTime() - lastUsedArray.get(command).getTime();

    var timeDiff = dif / 1000;

    if (timeDiff < commandCooldown) {
      console.log("NOT COOLED DOWN");
      return false;
    } else {
      return true;
    }
  } else {
    return true;
  }
}

// Called every time a message comes in
function onMessageHandler(target, context, msg, self) {
  if (self) { return; } // Ignore messages from the bot

  // Remove whitespace from chat message
  const commandName = msg.trim().toLowerCase();

  if (commandName == HELLO_VECTOR) {
    if (!checkCooldown(HELLO_VECTOR, HELLO_VECTOR_COOLDOWN)) {
      return;
    }

    const { spawn } = require('child_process');
    var username = String(context.username);
    var pp = spawn('python', ["hello.py", "--user", username]);

    lastUsedArray.set(HELLO_VECTOR, new Date());

    pp.stdout.on('data', (data) => {
      console.log(`stdout: ${data}`);
    });

    pp.stderr.on('data', (data) => {
      console.log(`stderr: ${data}`);
    });

    pp.on('close', (code) => {
      console.log(`child process exited with code ${code}`);
    });
  } else if (commandName === WHEELIE) {
    if (!checkCooldown(WHEELIE, WHEELIE_COOLDOWN)) {
      return;
    }

    const { spawn } = require('child_process');
    var pp = spawn('python', ["wheelie.py"]);

    pp.stdout.on('data', (data) => {
      console.log(`stdout: ${data}`);
    });

    pp.stderr.on('data', (data) => {
      console.log(`stderr: ${data}`);
    });

    pp.on('close', (code) => {
      console.log(`child process exited with code ${code}`);
    });
  } else if (commandName.includes(EYE_COLOUR)) {
    if (!checkCooldown(EYE_COLOUR, EYE_COLOUR_COOLDOWN)) {
      return;
    }

    //get colour from string
    var colour = commandName.substring(EYE_COLOUR.length);

    //post to colour file
    const { spawn } = require('child_process');
    var pp = spawn('python', ["eye_color.py", "--colour", colour]);

    pp.stdout.on('data', (data) => {
      console.log(`stdout: ${data}`);
    });

    pp.stderr.on('data', (data) => {
      console.log(`stderr: ${data}`);
    });

    pp.on('close', (code) => {
      console.log(`child process exited with code ${code}`);
    });
  } else {
    console.log(`* Unknown command ${commandName}`);
  }
}

// Called every time the bot connects to Twitch chat
function onConnectedHandler(addr, port) {
  console.log(`* Connected to ${addr}:${port}`);
}