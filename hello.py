import sys
import anki_vector
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--user")

    args = anki_vector.util.parse_command_args(parser)
    with anki_vector.Robot(args.serial) as robot:
        robot.behavior.say_text("Hello" + sys.argv[2])

if __name__ == "__main__":
    main()